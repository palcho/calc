SHELL = /bin/bash
PARSER = calc

run: $(PARSER)
	./$(PARSER)

$(PARSER): lex.yy.c y.tab.c y.tab.h
	gcc lex.yy.c y.tab.c -O3 -lm -o $@

y.tab.c y.tab.h: $(PARSER).y
	yacc -v -d  $(PARSER).y

lex.yy.c: $(PARSER).l
	lex $(PARSER).l

clean:
	-rm $(PARSER) lex.yy.c y.tab.c y.tab.h y.output
