%{
    void yyerror (char *str);
/* C declarations used in actions */
#   include <stdio.h> 
#   include <stdlib.h>
#   include <string.h>
#   include <inttypes.h>
#   include <math.h>
#   define print(...) fprintf(stderr, __VA_ARGS__)

    extern int yylex (void);
    double variable_value (int variable);
    void update_variable (int variable, double value);

    double variables[26];
%}
/* yacc definitions */
%union { double num; int id; } /* types that may be returned */
%start line               /* start token */
%token exit_command
%token <num> NUMBER       /* type of terminals (on the right side of the grammar) */
%token <id> VARIABLE FUNCTION
%type <num> line expr term assignment call /* type of non-terminals on the left side of the grammar */
%type <id> definition param 
%left '+' '-'             /* orders of precedence */
%left '*' '/' '%'
%right '^'
%left UMINUS UPLUS        /* unary minus */

%%

/* description of expected inputs :  corresponding actions (in C) */
line       : assignment '\n'        { print("%g\n>> ", $1); }
           | line assignment '\n'   { print("%g\n>> ", $2); }
           | definition '\n'        { print("%c defined\n>> ", $1); } /* @TODO */
           | line definition '\n'   { print("\n>> "); } /* @TODO */
           | expr '\n'              { print("%g\n>> ", $1); }
           | line expr '\n'         { print("%g\n>> ", $2); }
           | '\n'                   { print(">> "); }
           | line '\n'              { print(">> "); }
           | exit_command '\n'      { exit(0); }
           | line exit_command '\n' { exit(0); }
           ;

assignment : VARIABLE '=' expr      { update_variable($1, $3); $$ = $3; }
           ;

/*
   either use a bool to sort between function definition expression
   or create a expr' that mimics expr but builds ast's
*/
definition : FUNCTION '(' param ')' '=' expr  { $$ = $1; } /* @TODO */
           ;
param      : VARIABLE               { ; } /* TODO */
           | param ',' VARIABLE     { ; } /* TODO */
           ;

call       : FUNCTION '(' input ')' { ; } /* TODO */
           ;
input      : expr                   { ; } /* TODO */
           | input ',' expr         { ; } /* TODO */
           ;

expr       : expr '+' expr          { $$ = $1 + $3; }
           | expr '-' expr          { $$ = $1 - $3; }
           | expr '*' expr          { $$ = $1 * $3; }
           | expr '/' expr          { $$ = $1 / $3; }
           | expr '%' expr          { $$ = fmod($1, $3); }
           | expr '^' expr          { $$ = pow($1, $3); }
           | '-' expr %prec UMINUS  { $$ = - $2; }
           | '+' expr %prec UPLUS   { $$ = $2; }
           | '(' expr ')'           { $$ = $2; }
           | call                   { ; } /* TODO */
           | term                   { $$ = $1; }
           ;

term       : NUMBER                 { $$ = $1; }
           | VARIABLE               { $$ = variable_value($1); }
           ;

%%

/* C code */
double variable_value (int variable) {
    return variables[variable - 'a'];
}

void update_variable (int variable, double value) {
    variables[variable - 'a'] = value;
}

int main (void) {
    memset(variables, 0, sizeof(variables));
    print(">> ");
    return yyparse();
}

void yyerror (char *str) {
    fprintf(stderr, "%s\n", str);
}
