%{
#   include "y.tab.h"
    extern int yyerror (char *str);
%}

%%

(exit|quit)                          { return exit_command; }
[a-z]                                { yylval.id = *yytext; return VARIABLE; }
[A-Z]                                { yylval.id = *yytext; return FUNCTION; }
[0-9]+(\.[0-9]+)?([eE][+-]?[0-9]+)?  { yylval.num = strtod(yytext, NULL); return NUMBER; }
[ \t\r]                              { ; }
[-+*/%=^(),\n]                       { return *yytext; }
.                                    { ECHO; yyerror("unexpected character"); }

%%

int yywrap (void) {
    return 1;
}
